<?php

namespace App\DataFixtures;

use App\Entity\Continent;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;


class ContinentFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       // $product = new Product();
        // $manager->persist($product);
        $faker = Faker::create();

        $continents= array('Eurasie','Afrique','Antarctique','Amérique du Sud','Amérique du Nord');
        for($i=0; $i<5; $i++){
            $continent=new Continent();
            $continent->setName($continents[$i]);
           
            $this->addReference("continent$i", $continent);
            $manager->persist($continent);

        }     

        
        
        $manager->flush();
    }
}



