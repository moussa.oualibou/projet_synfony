<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Faker\Factory as Faker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class ArticleFixtures extends Fixture implements DependentFixtureInterface

{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker::create();

        for($i = 0; $i<50; $i++){
            $article=new Article();
            $article->setTitre( $faker->text(100) );
            $article->setImage( 'default.jpg');
            $article->setDescription( $faker->text(200) );


            $randomContinent = random_int(0, 4);
            $continent = $this->getReference("continent$randomContinent");
            
            //associer un continent aux articles
            $article->setContinent($continent);

            $manager->persist($article);
        }
        
        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            ContinentFixtures::class
        ];
    }




}