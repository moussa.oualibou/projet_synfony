<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Continent;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleContoller extends AbstractController
{
    /**
     * @Route("/ ", name="article.home")
     */
    public function home(ArticleRepository $articleRepository):Response
    {
         
		
		$results = $articleRepository->findBy([], [], 4, 4);
        //dd($results);
        return $this->render('article/home.html.twig', [
			'results' => $results
        ]);
    }

    /**
     * @Route("/article/{page}", name="article.index")
     */
    public function index(ArticleRepository $articleRepository, int $page = 1):Response
    {
        $articlesPerPage = $this->getParameter('articles_per_page');
		$totalArticles = $articleRepository->count([]);
		$results = $articleRepository->findBy([], [], $articlesPerPage, (--$page * $articlesPerPage));
        //dd($results);
        return $this->render('article/index.html.twig', [
            'articlesPerPage' => $articlesPerPage,
			'totalArticles' => $totalArticles,
			'results' => $results
        ]);
    }

    /**
	 * @Route("/article/details/{titre}", name="article.details")
	 */

	public function details(string $titre, ArticleRepository $articleRepository):Response
	{
		/*
		 * findOneBy nécessite une liste de conditions sur les propriétés de l'entité
		 *      clé est l'une des propriétés de l'entité
		 */
        
		$article = $articleRepository->findOneBy([
            'titre' => $titre
            
		]);
        
		return $this->render('article/details.html.twig', [
			'article' => $article
		]);
    }
     /**
	 * @Route("/article/form/create", name="article.create")
	 * @Route("/article/form/update/{id}", name="article.create.update")
	 */
    public function form(Request $request, EntityManagerInterface $entityManager)
	{
		// affichage d'un formulaire
		$article = new Article();
        $form = $this->createFormBuilder($article)
                     ->add('titre',TextType::class,[
                        'constraints' => [
                            new NotBlank([
                                'message' => "Le titre est obligatoire",
                            ],

                            )
                        
                        ],
                        'attr' => [
                            'placeholder'=>"contenu de l'article",
                            'class'=>'form-control'
                        ]
                    ])
                     ->add('image',TextareaType::class, [
                        'attr' => [
                            'placeholder'=>"la discription de l'article",
                            'class'=>'form-control']
                    ])
                     ->add('description', TextareaType::class, [
                        'constraints' => [
                            new NotBlank([
                                 'message' => "La description est obligatoire"
                             ])
                        ],
                        'attr' => [
                            'placeholder'=>"la discription de l'article",
                            'class'=>'form-control']
                    ])
                    
                    ->getForm();
        $form->handleRequest($request);
        dump($article);
        
        if($form->isSubmitted() && $form->isValid()){
            
            $entityManager->persist($article);
            $entityManager->flush();
			// redirection
			return $this->redirectToRoute('article.home');
		}
                
	

		return $this->render('admin/article.html.twig', [
			'form' => $form->createView()
		]);
	}


}
